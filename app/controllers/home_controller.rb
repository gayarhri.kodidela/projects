class HomeController < ApplicationController
  def index
  end

  def get_started
  end

  def creators
  end

  def sponsors
  end

  def privacy
  end
end
