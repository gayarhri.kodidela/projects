# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
USERS = [
  {
    email: 'user@worldofmind.org',
    username: 'user',
    password: 'user42'
  },
  {
    email: 'user2@worldofmind.org',
    username: 'user2',
    password: 'user42'
  }
]

ADMINS = [
  {
    email: 'admin@worldofmind.org',
    username: 'admin',
    password: 'admin42'
  },
  {
    email: 'admin2@user.org',
    username: 'admin2',
    password: 'admin42'
  }
]

PROJECTS = [
  {
    title: "World of Mind",
    url: "www.worldofmind.org",
    vision: "A decentral community and organization platform",
    description: "Our goal is to empower great communities and help them with a lot of features and tools. It does no matter if you want to grow a new community or improve the organization of a large one. The Mind plattorm grows with you.",
    public: true,
    user_id: 1
  }
]

CHILD_PROJECTS = [
  {
    parent_title: "World of Mind",
    project_attribtues: {
      title: "Quests",
      url: "http://quests.worldofmind.org",
      vision: "Stop working. Start playing.",
      description: "Quests is the worlds first platform where you can earn IOTA for solving issues all around the world. No traveling. No contracts. Not even a sign-up. Just do it!",
      public: true,
      user_id: 1
    }
  },
  {
    parent_title: "World of Mind",
    project_attribtues: {
      title: "Events",
      url: "http://events.worldofmind.org",
      vision: "Plan and organize your Event!",
      description: "Events is the world first platform to plan, create, publish and manage Events on IOTA's Tangle.",
      public: true,
      user_id: 1
    }
  },
]

def create_users
  USERS.each do |user_params|
    user = User.create!(user_params)
    user.confirm
    pp "user - #{user.username} created!"
  end
end

def create_admins
  ADMINS.each do |admin_params|
    admin = User.create!(admin_params)
    admin.admin = true
    admin.confirm
    pp "admin - #{admin.username} created!"
  end
end

def create_projects
  PROJECTS.each do |project_params|
    project = Project.create!(project_params)
    pp "project - #{project.title} created!"
  end
end

def create_child_projects
  CHILD_PROJECTS.each do |child_project|
    project_parent = Project.find_by_title(child_project[:parent_title])
    child_project = Project.new(child_project[:project_attribtues])
    child_project.parent = project_parent
    child_project.save!
    pp "child_project - #{child_project.title} with parent #{project_parent.title} created!"
  end
end

create_users
create_admins
create_projects
create_child_projects
